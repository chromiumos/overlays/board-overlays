This file should always contain information regarding which
branch/commit the firmware was built from.

repo : https://chrome-internal.googlesource.com/chromeos/third_party/sound-open-firmware-private
branch : cavs2.5-001-drop-stable
commit id : cdb124addcd4c6b084f933a5bd4eeb594c399f21

MD5sums:
b04927ff2326d2f4ead414e7ea2f1cf9  sof-adl.ldc
f87f96697268cd11f7809f95617a1f9f  sof-adl.ri
