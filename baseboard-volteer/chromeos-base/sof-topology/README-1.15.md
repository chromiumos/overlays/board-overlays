This file should contain the repo/branch/commit information about the repository
from which the included topology file has been created.

repo      : https://chrome-internal-review.googlesource.com/q/project:chromeos%252Fthird_party%252Fsound-open-firmware-private
branch    : cros-internal/tgl-012-drop-stable
commit id : 5b01794647310d3b890ac44cd88b837f776796d2
pdm1/sof-tgl-max98357a-rt5682.tplg is copied from build_tools/topology/sof-tgl-max98357a-rt5682-pdm1.tplg
pdm1-drceq/sof-tgl-max98357a-rt5682.tplg is copied from build_tools/topology/sof-tgl-max98357a-rt5682-pdm1-drceq.tplg

md5sums
63624fec232bcb837573837c3379022c  sof-tgl-max98357a-rt5682.tplg
e337343cb55b72d605e634773167dfaf  sof-tgl-max98373-rt5682.tplg
0f96e366a65be1cf571ec4607e269e44  sof-tgl-rt1011-rt5682.tplg
052e036a414d1948c2c104a3a9c5bdc3  sof-tgl-rt5682-ssp0-max98373-ssp2.tplg
f4e94a937643738a1ae212ef3adf6a2e  sof-tgl-sdw-max98373-rt5682.tplg
8054dcc507e2e7a098314f6686cf3dee  pdm1/sof-tgl-max98357a-rt5682.tplg
dcbc0067776edd724c202b260c8e0945  pdm1-drceq/sof-tgl-max98357a-rt5682.tplg
