This file should always contain information regarding which branch/commit
the firmware was built from.

Release build from branch cros-internal/cavs2.5-001-drop-stable for TGL

repo      : https://chrome-internal-review.googlesource.com/q/project:chromeos%252Fthird_party%252Fsound-open-firmware-private
branch    : cros-internal/cavs2.5-001-drop-stable
commit id : 504b41685e34dfeacbba05dc04e6b3326a57d943

Md5sum
81ff092dac2934746d818fb4e20b6254  sof-tgl.ldc
c19b068268dee604af5b8c8e8c510910  sof-tgl.ri
