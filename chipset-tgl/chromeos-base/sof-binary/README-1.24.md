This file should always contain information regarding which branch/commit
the firmware was built from.

Release v12 for TGL

repo      : https://chrome-internal-review.googlesource.com/q/project:chromeos%252Fthird_party%252Fsound-open-firmware-private
branch    : cros-internal/tgl-012-drop-stable
commit id : 0d9733c55cb4b0c810ad74c30cb174337d2c02e8

Md5sum
51c6e9bd6885a243b580684bb501cde8  sof-tgl.ldc
7cf2fa2d16a50f54df31412d4d3689cb  sof-tgl.ri
