# Copyright 2021 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

description   "Override CBI"
author        "chromium-os-dev@chromium.org"

start on started system-services
task
oom score -100   # Short task at startup; don't oom kill

script
  set -e

  sku="$(ectool cbi get 2 | grep 'As uint:' | cut -d' ' -f3)"

  # Fix is only applicable to SKU ID 0x50000
  if [ "$sku" != 327680 ] ; then
    exit 0
  fi

  fw_config="$(ectool cbi get 6 | grep 'As uint:' | cut -d' ' -f3)"
  new_fw_config=151605  # 0x25035

  if [ "$fw_config" -eq "$new_fw_config" ] ; then
    # Nothing to change
    exit 0
  fi

  logger -t "${UPSTART_JOB}" "Updating CBI fw_config to ${new_fw_config}"
  syslog-cat --identifier="${UPSTART_JOB}" -- ectool cbi set 6 "${new_fw_config}" 4
end script
