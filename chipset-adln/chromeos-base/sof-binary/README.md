This file should always contain information regarding which
branch/commit the firmware was built from.

CAVS2.5-001 drop stable

repo : https://chrome-internal.googlesource.com/chromeos/third_party/sound-open-firmware-private
branch : cavs2.5-001-drop-stable
commit id : 25d9941510829c738922b206fe21a0badb1eb35f

MD5sums:
87c9f40a092c772bbf503ce83f7e0ffd  sof-adl-n.ldc
8f61e3d1796f617363671eb3c45e75fe  sof-adl-n.ri
