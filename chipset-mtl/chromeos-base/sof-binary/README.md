This file should always contain information regarding which
branch/commit the firmware was built from.

MTL-008 SOF FW release.

repo : https://github.com/thesofproject/sof
branch : mtl-008-drop-stable
commit id : 90ca1ae7060dfbd22c376c5dd052b0630cf7f5a4

MD5sums:
716ef08d3aed9b1b71ef98bb123aec7f  sof-mtl.ri
