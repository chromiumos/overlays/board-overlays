This file should always contain information regarding which
branch/commit the firmware was built from.

Brox topology bundle that support multiple models.

repo      : https://chrome-internal.googlesource.com/chromeos/third_party/sound-open-firmware-private/
branch    : cavs2.5-001-drop-stable
Commit id : 9a87832712956e9e32f9f94da3ad52bb0212b681

Renaming sof-adl-rt1019-rt5682.tplg to sof-rpl-rt1019-rt5682.tplg

Md5sums:
9fc2792b84cbe7e85d99d19f29d84c59  sof-hda-generic-2ch.tplg
cccbc85342c3f63632477a93c918a00b  sof-hda-generic-4ch.tplg
cf9740cdcd569d14ebd64dcc5d105cf9  sof-rpl-rt1019-rt5682.tplg
