This file should always contain information regarding which branch/commit
the firmware was built from.

repo      : https://github.com/thesofproject/sof/
branch    : amd-rmb-stable
commit id : 114af199db6ea69e0e978bfed3f519d2540f8ed6

Md5sum
cb8b568c59463d4c7ef81a41a44e149a  sof-acp-rmb.tplg
d0b897b742f1bc826cf45547810570eb  sof-rmb-nau8825-max98360.tplg
