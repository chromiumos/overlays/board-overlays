JSL-005 release.
The topology enabled MCLK always on feature (PR:5952 on SOF repo).

This file should always contain information regarding which
branch/commit the firmware was built from.

repo      : https://github.com/thesofproject/sof
branch    : jsl-005-drop-stable
commit id : dd08b24e104c3ad7e1ff4136d8bb3ce5c79ad7c9

sof-jsl-rt5682.tplg
commit id : 7dfe578dfc673ee9d19583f557884585a8eb11df

sof-jsl-rt5650.tplg
commit id : 6dc35044e6af98197e79c1e49c8e08b365312540

md5sum:
2e69869f96c5d7b6468a25af839e051b  sof-jsl-cs42l42-mx98360a.tplg
081a3a207cb8ec5b44db49d51bfb6ee1  sof-jsl-da7219-mx98360a.tplg
0ba2e35484d4596c7cab8fffd11e4a94  sof-jsl-rt5650.tplg
6a7f2c13c3de46da1ae3add8d85f04b6  sof-jsl-rt5682-mx98360a.tplg
a0b5523677f4ef3981c413886f9cecd8  sof-jsl-rt5682-rt1015.tplg
1df0ec7f0a94b2c9140c53961ef42e26  sof-jsl-rt5682-rt1015-xperi.tplg
44cb007729c115a89790025215beabe2  sof-jsl-rt5682.tplg
