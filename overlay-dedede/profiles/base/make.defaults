# Copyright 2019 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

# Initial value just for style purposes.
LINUX_FIRMWARE=""
USE=""

USE="${USE} dedede"
USE="${USE} cros_ec"

# Comment these lines to de-activate the serial port.
# Enable kernel serial drivers
USE="${USE} pcserial"
# Declare set of enabled consoles
TTY_CONSOLE="ttyS0"

# Enable background blur.
USE="${USE} background_blur"

# Intel HrP2 wifi firmware
LINUX_FIRMWARE="${LINUX_FIRMWARE} iwlwifi-QuZ"

# Realtek Ethernet firmware
LINUX_FIRMWARE="${LINUX_FIRMWARE} rtl8168h-2"

# Enable tpm2
USE="${USE} -tpm tpm2 cr50_onboard"

# Include the elan_i2chid firmware updating tool
INPUT_DEVICES="${INPUT_DEVICES} elan_i2chid"

# Include goodix firmware updating tool
INPUT_DEVICES="${INPUT_DEVICES} goodix"

# Include g2touch firmware updating tool
INPUT_DEVICES="${INPUT_DEVICES} g2touch"

# Include pixart firmware updating tool
INPUT_DEVICES="${INPUT_DEVICES} pixart"

# Include weida firmware updating tool
INPUT_DEVICES="${INPUT_DEVICES} weida"

# Include the zinitix firmware updating tool
INPUT_DEVICES="${INPUT_DEVICES} zinitix"

# Include the wacom firmware updating tool
INPUT_DEVICES="${INPUT_DEVICES} wacom"

# Include the himax firmware updating tool
INPUT_DEVICES="${INPUT_DEVICES} himax"

# Enable slow boot notification during firmware updates
USE="${USE} enable_slow_boot_notify"

# chromeos-base/chromeos-config-bsp is located in this overlay.
USE="${USE} has_chromeos_config_bsp"

# Enable inference accuracy ml benchmarking
USE="${USE} inference_accuracy_eval"

# Disable subpixel rendering because it causes visual artifacts for rotated
# displays.
USE="${USE} -subpixel_rendering"

# Enable Shimless RMA
USE="${USE} rmad"

# Enable cecservice
USE="${USE} cecservice"

# Enable Crosier testing support.
USE="${USE} crosier_binary"

# Use cros-camera-algo service
USE="${USE} cros_camera_algo"

# Use Vulkan for raster/composite in Chrome
USE="${USE} vulkan_chrome"

# Disable video decode batching in the renderer
USE="${USE} disable_video_decode_batching"
