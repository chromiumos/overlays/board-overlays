This file should always contain information regarding which
branch/commit the firmware was built from.

CAVS2.5-001 drop stable

repo : https://chrome-internal.googlesource.com/chromeos/third_party/sound-open-firmware-private
branch : cavs2.5-001-drop-stable
commit id : 504b41685e34dfeacbba05dc04e6b3326a57d943

MD5sums:
81ff092dac2934746d818fb4e20b6254  sof-adl.ldc
c19b068268dee604af5b8c8e8c510910  sof-adl.ri
