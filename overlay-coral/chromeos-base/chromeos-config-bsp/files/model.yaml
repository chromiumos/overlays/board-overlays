#
# Copyright 2018 The ChromiumOS Authors
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
imports:
  - "include/audio.yaml"

rmad-common: &rmad_common
  enabled: True
  has-cbi: False
  use-legacy-custom-label: True

camera-base-config: &camera_base_config
  count: 1
  devices:
    - interface: "usb"
      facing: "front"
      orientation: 0
      flags:
        support-autofocus: False
        support-1080p: False
  legacy-usb: True

camera-two-override-config: &camera_two_override_config
  count: 2
  devices:
    - interface: "usb"
      facing: "front"
      orientation: 0
      flags:
        support-autofocus: False
        support-1080p: False
    - interface: "usb"
      facing: "back"
      orientation: 0
      flags:
        support-autofocus: False
        support-1080p: False
  legacy-usb: True

device-config: &device_config
  $dptf-device: "{{$device-name}}"
  $touchpad-wakeup: "1"
  $test-label: "{{$device-name}}"
  name: "{{$device-name}}"
  audio:
    main: *audio_common
  camera: *camera_base_config
  firmware:
    no-firmware: True
  test-label: "{{$test-label}}"
  power:
    charging-ports: |
      CROS_USB_PD_CHARGER0 LEFT
      CROS_USB_PD_CHARGER1 RIGHT
    low-battery-shutdown-percent: "4.0"
    power-supply-full-factor: "0.94"
    suspend-to-idle: "1"
    touchpad-wakeup: "{{$touchpad-wakeup}}"
  thermal:
    dptf-dv: "{{$dptf-device}}/dptf.dv"
    files:
      - source: "{{$dptf-device}}/thermal/dptf.dv"
        destination: "/etc/dptf/{{$dptf-device}}/dptf.dv"
  ui:
    help-content-id: "{{$help-content-id}}"
  identity: &base_ident
    platform-name: "Coral"
    frid: "Google_Coral"
    sku-id: "{{$sku-id}}"
  hardware-properties: &hardware_properties
    $form-factor: CHROMEBOOK
    form-factor: "{{$form-factor}}"
  rmad: *rmad_common

convertible-base-config: &convertible_base_config
  <<: *device_config
  hardware-properties:
    <<: *hardware_properties
    form-factor: CONVERTIBLE
    is-lid-convertible: True
    has-lid-accelerometer: True
    has-base-accelerometer: True
    has-base-gyroscope: True

convertible-config: &convertible_config
  <<: *convertible_base_config

wl-device-config: &wl_device_config
  <<: *device_config
  identity:
    <<: *base_ident
    custom-label-tag: "{{$custom-label-tag}}"

wl-convertible-config: &wl_convertible_config
  <<: [ *wl_device_config, *convertible_base_config ]
  hardware-properties:
    <<: *hardware_properties
    form-factor: CONVERTIBLE
    is-lid-convertible: True
    has-lid-accelerometer: True
    has-base-accelerometer: True
    has-base-gyroscope: True

astronaut-config: &astronaut_config
  <<: *device_config

chromeos:
  devices:
    - $device-name: "astronaut"
      $help-content-id: "ASTRONAUT"
      skus:
        - $sku-id: 0
          config: *astronaut_config
        - $sku-id: 1
          config: *astronaut_config
        - $sku-id: 61
          config:
            <<: *astronaut_config
            modem:
              firmware-variant: "{{$device-name}}"
              modem-type: 1
        - $sku-id: 62
          config:
            <<: *astronaut_config
            modem:
              firmware-variant: "{{$device-name}}"
              modem-type: 1
    - $device-name: "babymega"
      $ucm-device: "babymega"
      $ucm-target-suffix: "Babymega"
      $help-content-id: "BABYMEGA"
      products:
        - $custom-label-tag: ""
        - $custom-label-tag: "kitefin"
      skus:
        - $sku-id: 52
          config: *wl_device_config
        - $sku-id: 53
          config: *wl_device_config
    - $device-name: "babytiger"
      $ucm-device: "babytiger"
      $ucm-target-suffix: "Babytiger"
      $ucm: "2mic"
      $help-content-id: "BABYTIGER"
      products:
        - $custom-label-tag: ""
        - $custom-label-tag: "longfin"
      skus:
        - $sku-id: 30
          config: *wl_device_config
        - $sku-id: 33
          config: *wl_device_config
    - $device-name: "blacktip"
      $dptf-device: "astronaut"
      products:
        - $custom-label-tag: "" # default
          $help-content-id: "BLACKTIP"
        - $custom-label-tag: "ctl"
          $help-content-id: "BLACKTIP-CTL"
        - $custom-label-tag: "gsa"
          $help-content-id: "BLACKTIP-GSA"
        - $custom-label-tag: "positivo"
          $help-content-id: "BLACKTIP-POSITIVO"
        - $custom-label-tag: "lanix"
          $help-content-id: "BLACKTIP-LANIX"
        - $custom-label-tag: "sns"
          $help-content-id: "BLACKTIP-SNS"
        - $custom-label-tag: "multilaser"
          $help-content-id: "BLACKTIP-MULTILASER"
        # Workaround to make it compatible with "ctl"
        - $custom-label-tag: "to-be-determined-bt-loem1"
          $help-content-id: "BLACKTIP-CTL"
      skus:
        - $sku-id: 36
          config: *wl_device_config
    - $device-name: "blacktip360"
      $ucm: "2mic"
      $touchpad-wakeup: "0"
      $dptf-device: "astronaut"
      products:
        - $custom-label-tag: "" # default
          $help-content-id: "BLACKTIP360"
        - $custom-label-tag: "ctl"
          $help-content-id: "BLACKTIP360-CTL"
        - $custom-label-tag: "gsa"
          $help-content-id: "BLACKTIP360-GSA"
        - $custom-label-tag: "xma"
          $help-content-id: "BLACKTIP360-XMA"
        - $custom-label-tag: "positivo"
          $help-content-id: "BLACKTIP360-POSITIVO"
        - $custom-label-tag: "lanix"
          $help-content-id: "BLACKTIP360-LANIX"
        - $custom-label-tag: "sns"
          $help-content-id: "BLACKTIP360-SNS"
        - $custom-label-tag: "multilaser"
          $help-content-id: "BLACKTIP360-MULTILASER"
        # Workaround to make it compatible with "ctl"
        - $custom-label-tag: "to-be-determined-bt-loem1"
          $help-content-id: "BLACKTIP360-CTL"
      skus:
        - $sku-id: 37
          config:
            <<: *wl_convertible_config
            camera: *camera_two_override_config
        - $sku-id: 38
          config: *wl_convertible_config
    - $device-name: "blacktiplte"
      $dptf-device: "astronaut"
      products:
        - $custom-label-tag: "default" # default
          $help-content-id: "BLACKTIPLTE-DEFAULT"
        - $custom-label-tag: "ctl"
          $help-content-id: "BLACKTIPLTE-CTL"
        - $custom-label-tag: "gsa"
          $help-content-id: "BLACKTIPLTE-GSA"
      skus:
        - $sku-id: 65
          config:
            <<: *wl_device_config
            modem:
              firmware-variant: "{{$device-name}}"
              modem-type: 1
    - $device-name: "blue"
      $dptf-device: "astronaut"
      $help-content-id: "BLUE"
      skus:
        - $sku-id: 6
          config: *device_config
        - $sku-id: 7
          config: *device_config
        - $sku-id: 12
          config: *device_config
    - $device-name: "bruce"
      $touchpad-wakeup: "0"
      $dptf-device: "common"
      $help-content-id: "BRUCE"
      skus:
        - $sku-id: 8
          config: *convertible_config
        - $sku-id: 11
          $ucm: "2mic"
          $ucm-device: "bruce"
          $ucm-target-suffix: "Bruce360"
          config: *convertible_config
    - $device-name: "epaulette"
      $help-content-id: "EPAULETTE"
      $dptf-device: "astronaut"
      # TODO(epaulette-odm): Modify the current conf files as
      # required, they are copies from other 2-mic model.
      $ucm: "2mic"
      $ucm-device: "epaulette"
      $ucm-target-suffix: "Epaulette"
      # TODO(epaulette-odm): Add validated dptf.dv file into
      # BSP, it's currently using inherited version from the parent.
      skus:
        - $sku-id: 13
          config: *device_config
        - $sku-id: 14
          config: *device_config
        - $sku-id: 15
          config: *device_config
        - $sku-id: 16
          config: *device_config
    - $device-name: "lava"
      $help-content-id: "LAVA"
      $touchpad-wakeup: "0"
      $dptf-device: "astronaut"
      skus:
        - $sku-id: 4
          config: *convertible_config
        - $sku-id: 5
          config: *convertible_config
        - $sku-id: 9
          config: *convertible_config
        - $sku-id: 10
          config: *convertible_config
    - $device-name: "nasher"
      $help-content-id: "NASHER"
      $ucm: ""
      $ucm-suffix-path: ""
      $ucm-device: "nasher"
      $ucm-target-suffix: "nasher"
      skus:
        - $sku-id: 160
          config: *device_config
        - $sku-id: 161
          config: *device_config
        - $sku-id: 162
          config: *device_config
    - $device-name: "nasher360"
      $help-content-id: "NASHER360"
      $cras: "nasher"
      $ucm-device: "nasher360"
      $ucm: "2mic"
      $ucm-target-suffix: "Nasher360"
      $touchpad-wakeup: "0"
      $dptf-device: "nasher"
      skus:
        - $sku-id: 163
          $ucm: "nasher"
          config: *convertible_config
        - $sku-id: 164
          $ucm: "nasher"
          config: *convertible_config
        - $sku-id: 165
          $ucm: "2micNasher360"
          config: *convertible_config
        - $sku-id: 166
          $ucm: "2micNasher360"
          config:
            <<: *convertible_config
            camera: *camera_two_override_config
    - $device-name: "porbeagle"
      $help-content-id: "PORBEAGLE"
      $dptf-device: "astronaut"
      $cras: "astronaut"
      skus:
        - $sku-id: 26
          config: *device_config
        - $sku-id: 27
          config: *device_config
    - $device-name: "rabbid"
      $help-content-id: "RABBID"
      products:
        - $custom-label-tag: ""
        - $custom-label-tag: "sailfin"
      skus:
        - $sku-id: 28
          $cras: "rabbid_rugged"
          $ucm-device: "rabbid_rugged"
          $ucm: "2mic"
          $ucm-target-suffix: "Rabbid_rugged"
          config: *wl_device_config
        - $sku-id: 31
          $ucm-device: "rabbid"
          $ucm: "2mic"
          $ucm-target-suffix: "Rabbid"
          config: *wl_device_config
        - $sku-id: 32
          $ucm-device: "rabbid"
          $ucm: "2mic"
          $ucm-target-suffix: "Rabbid"
          config: *wl_device_config
    - $device-name: "robo"
      $help-content-id: "ROBO"
      skus:
        - $sku-id: 70
          config: *device_config
    - $device-name: "robo360"
      $help-content-id: "ROBO360"
      $cras: "nasher"
      $ucm: ""
      $ucm-suffix-path: ""
      $ucm-device: "robo360"
      $ucm-target-suffix: "robo360"
      $dptf-device: "robo"
      $touchpad-wakeup: "0"
      skus:
        - $sku-id: 71
          config:
            <<: *convertible_config
            camera: *camera_two_override_config
    - $device-name: "santa"
      $help-content-id: "SANTA"
      $cras: "astronaut"
      $dptf-device: "astronaut"
      skus:
        - $sku-id: 2
          config: *device_config
        - $sku-id: 3
          config: *device_config
    - $device-name: "whitetip"
      $test-label: "robo"
      products:
        - $custom-label-tag: "" # default
          $help-content-id: "WHITETIP"
        - $custom-label-tag: "ctl"
          $help-content-id: "WHITETIP-CTL"
        - $custom-label-tag: "prowise"
          $help-content-id: "WHITETIP-PROWISE"
        - $custom-label-tag: "ytl"
          $help-content-id: "WHITETIP-YTL"
        - $custom-label-tag: "sector5"
          $help-content-id: "WHITETIP-SECTOR5"
        - $custom-label-tag: "pcmerge"
          $help-content-id: "WHITETIP-PCMERGE"
        - $custom-label-tag: "xma"
          $help-content-id: "WHITETIP-XMA"
        - $custom-label-tag: "multilaser"
          $help-content-id: "WHITETIP-MULTILASER"
      skus:
        - $sku-id: 78
          config: *wl_device_config
        - $sku-id: 82
          config: *wl_device_config
