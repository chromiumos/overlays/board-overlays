This file should always contain information regarding which branch/commit
the firmware was built from.

repo      : https://github.com/thesofproject/sof/
branch    : MTK provided build in b:345381961#comment11
commit id : MTK provided build in b:345381961#comment11

Md5sum
f990bc7a8ba76a84c6148bdc9be34487  sof-mt8196-4ch.tplg
d1b923ca577ab9cd3e92f156bd7da550  sof-mt8196.tplg
