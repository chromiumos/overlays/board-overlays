# Copyright 2020 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Initial value just for style purposes.
USE=""
LINUX_FIRMWARE=""

# Google USBPD peripheral firmwares
# LINUX_FIRMWARE="${LINUX_FIRMWARE} cros-pd"

# Enable RealTek RTW8852A WiFi firmware.
LINUX_FIRMWARE="${LINUX_FIRMWARE} rtw8852a"

# Enable Realtek 8822AE USB BT firmware
LINUX_FIRMWARE="${LINUX_FIRMWARE} rtl_bt-8852ae-usb"

# Enable WCN6855
LINUX_FIRMWARE="${LINUX_FIRMWARE} ath11k_wcn6855"

# Enable WCN6856 USB BT firmware
LINUX_FIRMWARE="${LINUX_FIRMWARE} qca-wcn685x-bt"

USE="${USE} guybrush"

# Enable cr50 support
USE="${USE} -tpm tpm2 cr50_onboard"

# Allow Chrome to use Tablet mode
USE="${USE} touchview"

USE="${USE} unibuild has_chromeos_config_bsp"

USE="${USE} arc"

# Ability to run guest VMs.
USE="${USE} kvm_host crosvm-gpu virtio_gpu"

# Add ec firmware.
USE="${USE} cros_ec"

# Add EC logging
USE="${USE} eclog"

# Enable NVMe utility
USE="${USE} nvme"

# modemfwd is used for updating modem firmware.
USE="${USE} modemfwd"

# Enable console serial port
USE="${USE} tty_console_ttyS0"

# Enable serial console
TTY_CONSOLE="ttyS0"

# Add serial driver for fingerprint UART
USE="${USE} pcserial"

# Enable eMMC tools
USE="${USE} mmc"

# Enable miniOS support
USE="${USE} minios"

# Enable LVM stateful partition
USE="${USE} lvm_stateful_partition"

# Include the g2touch firmware updating tool
INPUT_DEVICES="${INPUT_DEVICES} g2touch"

# Include the elan firmware updating tool
INPUT_DEVICES="${INPUT_DEVICES} elan_i2chid"

# Disable Cr50 deep sleep during suspend
USE="${USE} cr50_disable_sleep_in_suspend"

# Enable Type-C Daemon
USE="${USE} typecd"

# Enable for daisydog
USE="${USE} watchdog"

# Disable signed PSP verstage
# TODO(b/330158069): Re-enable when the latest vboot version is supported.
USE="${USE} unsigned-psp-verstage"

# Enable single frame super resolution.
USE="${USE} camera_feature_super_res"

# Enable Crosier testing support.
USE="${USE} crosier_binary"

# Use ANGLE as the GL backend for cros_camera_service
USE="${USE} camera_angle_backend"

# Use Vulkan for raster/composite in Chrome
USE="${USE} vulkan_chrome"

# Disable video decode batching in the renderer
USE="${USE} disable_video_decode_batching"
