This file should always contain information regarding which branch/commit
the firmware was built from.

repo      : https://github.com/thesofproject/sof/
branch    : amd-rmb-stable
commit id : 98894ce4fb487863c615395109d87f50f6b5a297

Md5sum
71a1c493a3d7c6f13cf16c5edb71d4a9  sof-rmb.ldc
737de8297e0474f08751087fa4a860b1  sof-rmb.ri
