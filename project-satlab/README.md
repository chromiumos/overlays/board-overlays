# What is Satlab?

* Main Lab in a box
* Your own mini-fleet for testing: on-desk, internal distributed labs
* Typical use cases: accelerated bring-up, unstable hardware, testing the tests
* See go/satlab-in-a-nutshell
