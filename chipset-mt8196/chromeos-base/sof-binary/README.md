This file should always contain information regarding which branch/commit
the firmware was built from.

Base branch:
repo      : https://github.com/thesofproject/sof.git
branch    : mt8196/v0.1
commit id : c581151206c8bb80ddae1c7355ad4a1a37d8ccd4

Additional unmerged commits:
2 commits from the pull request https://github.com/thesofproject/sof/pull/9816
 * schedule: config to build LL without task rescheduling
 * mt8196: set config SCHEDULE_LL_NO_RESCHEDULE_TASK=y

Md5sum
aec6a4e74e249bf9de716e3b55ecb347  sof-mt8196.ri
