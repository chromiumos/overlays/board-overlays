Copyright 2022 The ChromiumOS Authors
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.


JSL-005 release

This update includes MCLK always-on feature.

This file should always contain information regarding which
branch/commit the firmware was built from.

repo      : https://github.com/thesofproject/sof
branch    : jsl-005-drop-stable
commit id : dd08b24e104c3ad7e1ff4136d8bb3ce5c79ad7c9

md5sum:
8da229c41df87d3396b01d6223791db9  sof-jsl.ldc
45d815054111d4ab216551f6fd00f67f  sof-jsl.ri
